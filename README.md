# README

## Pontos trabalhados

- Criado um endpoint imaginando que seria o ponto de entrada do payload.
- Serializers para esse payload, trabalhando os dados para salvar no banco.
- Uso do life-cycle do Modelo pra enviar as informações depois de salvar no banco.
- Uso de jsonb para salvar informações aninhadas. Modelagem que levou em consideração que não haveria o reuso dessas informações posteriormente dado a formatação dos dados.
- Use de um serializer de saída para formatação dos dados antes do envio das informações via serviço.

## Dificuldades encontradas

- Erro no payload de envio de exemplo, uma virgula sobrando dentro do Customer.

- API reclama que falta o 'total_shipping', fora do padrao camel case que deveria ser enviado. Levando a um "ajuste" para poder enviar como o app do heroku espera.

- Ao utilizar o Active Model Serializer fora do context de controller, acabei tendo que fazer a edição das chaves do josn de forma um pouco mais manual. Como podesse ver no método `format_json_to_send` no arquivo `app/services/send_order_service.rb`.
