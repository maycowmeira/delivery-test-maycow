# frozen_string_literal: true

require 'active_model_serializers/register_jsonapi_renderer'

class SendOrderService < BaseService
  include HTTParty
  base_uri 'delivery-center-recruitment-ap.herokuapp.com'

  def initialize(order)
    @order = order
    @order_json = format_json_to_send OrderSerializer.new(order)
  end

  def send
    self.class.post('',
                    headers: { 'X-Sent' => formated_sending_time },
                    body: @order_json)
  end

  def formated_sending_time
    Time.zone.now.localtime.strftime('%Hh%M - %d/%m/%y')
  end

  def format_json_to_send(serialized_order)
    serialized_order.as_json.deep_transform_keys do |key|
      key.to_s.camelize(:lower)
    end.merge(total_shipping: @order.total_shipping).to_json
  end
end
