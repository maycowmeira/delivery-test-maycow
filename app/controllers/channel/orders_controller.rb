# frozen_string_literal: true

class Channel::OrdersController < ApplicationController
  def create
    hash = Channel::OrderSerializer.new(params).serializable_hash(include: %i[items customer payments])

    Order.create(hash)

    render status: :ok
  end
end
