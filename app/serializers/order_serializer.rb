# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id              :bigint           not null, primary key
#  city            :string           not null
#  complement      :string           not null
#  country         :string           not null
#  customer        :jsonb            not null
#  delivery_fee    :string           not null
#  district        :string           not null
#  dt_order_create :datetime         not null
#  external_code   :string           not null
#  items           :jsonb            is an Array
#  latitude        :float            not null
#  longitude       :float            not null
#  number          :string           not null
#  payments        :jsonb            is an Array
#  postal_code     :string           not null
#  state           :string           not null
#  street          :string           not null
#  sub_total       :string           not null
#  total           :string           not null
#  total_shipping  :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer          not null
#

class OrderSerializer < ActiveModel::Serializer
  attributes :latitude, :longitude, :store_id, :city, :complement, :country, :delivery_fee, :district
  attributes :dt_order_create, :external_code, :number, :postal_code
  attributes :sub_total, :total, :total_shipping, :state, :street
  attributes :customer, :items, :payments

  def dt_order_create
    object.dt_order_create.to_datetime.strftime('%FT%T.%LZ')
  end
end
