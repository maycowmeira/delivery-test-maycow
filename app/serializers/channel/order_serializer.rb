# frozen_string_literal: true

class Channel::OrderSerializer
  include ActiveModel::Serialization
  include ActiveModel::Validations

  validates :external_code, :store_id, :total_shipping, :sub_total, :delivery_fee, :total, presence: true

  validates :country, :state, :city, :district, :street, :complement, :latitude, :longitude, presence: true

  validates :dt_order_create, :postal_code, :number, presence: true

  validates :customer, :items, :payments, presence: true

  attr_accessor :latitude, :longitude, :store_id, :city, :complement, :country, :delivery_fee, :district
  attr_accessor :dt_order_create, :external_code, :number, :postal_code
  attr_accessor :sub_total, :total, :total_shipping, :state, :street
  attr_accessor :customer, :items, :payments

  def initialize(attrs = {})
    @external_code = attrs['id']
    @store_id = attrs['store_id']

    @dt_order_create = DateTime.parse(attrs['date_created'])

    @latitude = attrs['shipping']['receiver_address']['latitude']
    @longitude = attrs['shipping']['receiver_address']['longitude']
    @complement = attrs['shipping']['receiver_address']['comment']
    @country = attrs['shipping']['receiver_address']['country']['id']
    @city = attrs['shipping']['receiver_address']['city']['name']
    @district = attrs['shipping']['receiver_address']['neighborhood']['name']
    @number = attrs['shipping']['receiver_address']['street_number']
    @postal_code = attrs['shipping']['receiver_address']['zip_code']
    @state = attrs['shipping']['receiver_address']['state']['name']
    @street = attrs['shipping']['receiver_address']['street_name']

    @delivery_fee = attrs['total_shipping']
    @total_shipping = attrs['total_shipping']
    @sub_total = attrs['total_amount']
    @total = attrs['total_amount_with_shipping']

    @customer = build_customer(attrs['buyer'])
    @items = fetch_items(attrs['order_items'])
    @payments = fetch_payments(attrs['payments'])
  end

  def attributes
    {
      latitude: nil, longitude: nil, store_id: nil, city: nil, complement: nil, country: nil, delivery_fee: nil,
      district: nil, dt_order_create: nil, external_code: nil, number: nil, postal_code: nil, sub_total: nil,
      total: nil, total_shipping: nil, state: nil, street: nil
    }
  end

  def build_customer(buyer)
    Channel::CustomerSerializer.new(buyer)
  end

  def fetch_items(order_items)
    order_items.map do |item|
      Channel::OrderItemSerializer.new(item)
    end
  end

  def fetch_payments(payments)
    payments.map do |payment|
      Channel::PaymentSerializer.new(payment)
    end
  end
end
