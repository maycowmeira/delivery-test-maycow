# frozen_string_literal: true

class Channel::CustomerSerializer
  include ActiveModel::Serialization
  include ActiveModel::Validations

  validates :external_code, :name, :email, :contact, presence: true

  attr_accessor :external_code, :name, :email, :contact

  def initialize(attrs = {})
    @external_code = attrs['id']
    @name = attrs['nickname']
    @email = attrs['email']
    @contact = attrs['phone']['area_code'] + attrs['phone']['number']
  end

  def attributes
    {
      external_code: nil, name: nil, email: nil, contact: nil
    }
  end
end
