# frozen_string_literal: true

class Channel::OrderItemSerializer
  include ActiveModel::Serialization
  include ActiveModel::Validations

  validates :external_code, :name, :price, :quantity, :total, :sub_items, presence: true

  attr_accessor :external_code, :name, :price, :quantity, :total, :sub_items

  def initialize(attrs = {})
    @external_code = attrs['item']['id']
    @name = attrs['item']['title']
    @price = attrs['unit_price'].to_f
    @quantity = attrs['quantity'].to_i
    @total = attrs['full_unit_price'].to_f
    @sub_items = attrs['sub_itens'] || []
  end

  def attributes
    {
      external_code: nil, name: nil, price: nil, quantity: nil, total: nil, sub_items: nil
    }
  end
end
