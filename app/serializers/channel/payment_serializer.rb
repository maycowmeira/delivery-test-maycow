# frozen_string_literal: true

class Channel::PaymentSerializer
  include ActiveModel::Serialization
  include ActiveModel::Validations

  validates :type, :value, presence: true

  attr_accessor :type, :value

  def initialize(attrs = {})
    @type = attrs['payment_type'].upcase
    @value = attrs['total_paid_amount'].to_f
  end

  def attributes
    {
      type: nil, value: nil
    }
  end
end
