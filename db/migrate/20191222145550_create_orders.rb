class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.float :latitude, null: false
      t.float :longitude, null: false
      t.integer :store_id, null: false

      t.datetime :dt_order_create, null: false

      t.string :city, null: false
      t.string :complement, null: false
      t.string :country, null: false
      t.string :delivery_fee, null: false
      t.string :district, null: false
      t.string :external_code, null: false
      t.string :number, null: false
      t.string :postal_code, null: false
      t.string :sub_total, null: false
      t.string :total, null: false
      t.string :total_shipping, null: false
      t.string :state, null: false
      t.string :street, null: false

      t.jsonb :customer, null: false
      t.jsonb :items, array: true, default: []
      t.jsonb :payments, array: true, default: []

      t.timestamps
    end
  end
end
