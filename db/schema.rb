# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_22_145550) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "orders", force: :cascade do |t|
    t.float "latitude", null: false
    t.float "longitude", null: false
    t.integer "store_id", null: false
    t.datetime "dt_order_create", null: false
    t.string "city", null: false
    t.string "complement", null: false
    t.string "country", null: false
    t.string "delivery_fee", null: false
    t.string "district", null: false
    t.string "external_code", null: false
    t.string "number", null: false
    t.string "postal_code", null: false
    t.string "sub_total", null: false
    t.string "total", null: false
    t.string "total_shipping", null: false
    t.string "state", null: false
    t.string "street", null: false
    t.jsonb "customer", null: false
    t.jsonb "items", default: [], array: true
    t.jsonb "payments", default: [], array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
