# frozen_string_literal: true

require 'test_helper'

class Channel::OrdersControllerTest < ActionDispatch::IntegrationTest
  describe :create do
    it 'should create a order by incoming hash from external channel' do
      VCR.use_cassette('controllers/channel/order_sending') do
        post channel_orders_path, params: channel_order_hash

        order = Order.find_by(external_code: '9987071')

        assert order.valid?

        assert_response :success
      end
    end

    it 'should raises error if wrong params is sent' do
      VCR.use_cassette('controllers/channel/order_sending_error') do
        assert_raises do
          post channel_orders_path, params: {}

          assert_response :error
        end
      end
    end
  end
end
